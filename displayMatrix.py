#!/usr/bin/env python
import sys
from relationships.relationshipMatrix import RelationshipMatrix
from Loader import Loader

fname = sys.argv[1]
mat = RelationshipMatrix(Loader(fname).polyhedrons).matrix
#print fname
print map(lambda x: map(str, x), mat)
