from Loader import Loader
from geometry.unproject import unproject
from Polygon.Utils import tileEqual

l = Loader('objs/DC nObs_c.obj')
a = l.polyhedrons[0]
b = l.polyhedrons[1]

#find an x,y for this. add it to viewport.py
#maybe rename viewport to 'useful list of constants'
#configure precision of tileEqual?
#is a theoretical maximum


#what does tile equal do with an empty arg
#if area is == 0 then false

mesh = a.projection() & b.projection()

if len(mesh) == 0:
  print "GET OUT, FALSE"

for poly in tileEqual(mesh,5,5):
  for cpoint in poly[-1]:
    #only get points from contours, contour is always last element of polygon 
    #what happens if a poly doesn't have contours?
    ret = unproject(a,cpoint[0],cpoint[1]) >= unproject(b,cpoint[0],cpoint[1])
    if not(isinstance(ret,bool)): #is a in front of b for contour point in poly 
      print ret[2]
    print False #why not print ret?

    
