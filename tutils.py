#
## tutils.py
## some utilities for helping test functionality
## author: Nathan Eloe
#

from relationships.relationshipMatrix import RelationshipMatrix
from Loader import Loader


#gets obscurations for a filename
def getObs(fname):
    polys = Loader(fname).polyhedrons
    matrix = RelationshipMatrix(polys).matrix
    return matrix[0][1].obscuration, matrix[1][0].obscuration


def getRCC(fname):
    polys = Loader(fname).polyhedrons
    matrix = RelationshipMatrix(polys).matrix
    return matrix[0][1].intersection, matrix[1][0].intersection


def getVRCC(fname):
    polys = Loader(fname).polyhedrons
    matrix = RelationshipMatrix(polys).matrix
    return matrix[0][1], matrix[1][0]
