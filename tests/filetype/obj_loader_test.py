import unittest

# Add the PyVRCC-3D root to the system path so that imports can be made
import os
import sys
pyvrcc3d_root = os.path.abspath(os.path.join(os.path.dirname(
                 os.path.abspath(__file__)), "..", ".."))
sys.path.append(pyvrcc3d_root)

from Loader import Loader

testingFiles = \
    {
    '../../newObjs/DC_cObs_c.obj': (2, 10, 9),
    '../../newObjs/DC_cObs.obj': (2, 1920, 964),
    '../../newObjs/DC_eObs_c.obj': (2, 1920, 964),
    '../../newObjs/DC_eObs.obj': (2, 1920, 964),
    '../../newObjs/DC_nObs_c.obj': (2, 8, 8),
    '../../newObjs/DC nObs_e.obj': (2, 1920, 964),
    '../../newObjs/DC_nObs.obj': (2, 1920, 964),
    '../../newObjs/DC pObs_c.obj': (2, 1920, 964),
    '../../newObjs/DC_pObs.obj': (2, 1920, 964),
    '../../newObjs/EC_cObs_c.obj': (2, 10, 9),
    '../../newObjs/EC_cObs.obj': (2, 1920, 964),
    '../../newObjs/EC_eObs_c.obj': (2, 1920, 964),
    '../../newObjs/EC_eObs.obj': (2, 12, 10),
    '../../newObjs/EC_nObs_c.obj': (2, 8, 8),
    '../../newObjs/EC nObs_e.obj': (2, 1920, 964),
    '../../newObjs/EC_nObs.obj': (2, 1920, 964),
    '../../newObjs/EC pObs_c.obj': (2, 1920, 964),
    '../../newObjs/EC_pObs.obj': (2, 1920, 964),
    '../../newObjs/EQ_eObs_e.obj': (2, 8, 8),
    '../../newObjs/NTPPC_cObs.obj': (2, 1920, 964),
    '../../newObjs/NTPP_cObs_c.obj': (2, 1920, 964),
    '../../newObjs/PO_cObs_c.obj': (2, 10, 9),
    '../../newObjs/PO cObs_e.obj': (2, 128, 68),
    '../../newObjs/PO_cObs.obj': (2, 1920, 964),
    '../../newObjs/PO eObs_c.obj': (2, 1088, 548),
    '../../newObjs/PO_eObs.obj': (2, 14, 11),
    '../../newObjs/PO pObs_c.obj': (2, 1920, 964),
    '../../newObjs/PO_pObs_e.obj': (2, 16, 12),
    '../../newObjs/PO_pObs.obj': (2, 1920, 964),
    '../../newObjs/TPPc cObs_e.obj': (2, 1920, 964),
    '../../newObjs/TPPc_eObs_e.obj': (2, 8, 8),
    '../../newObjs/TPPc eObs.obj': (2, 1920, 964),
    '../../newObjs/TPPc_eObs.obj': (2, 14, 11),
    '../../newObjs/TPP cObs_c.obj': (2, 1920, 964),
    '../../newObjs/TPP cObs_e.obj': (2, 1920, 964),
    '../../newObjs/TPP  eObs_c.obj': (2, 1920, 964),
    '../../newObjs/TPP_eObs_e.obj': (2, 8, 8),
    '../../objs/DC cObs_c.obj': (2, 3968, 1988),
    '../../objs/DC cObs.obj': (2, 3968, 1988),
    '../../objs/DC eObs_c.obj': (2, 3968, 1988),
    '../../objs/DC eObs.obj': (2, 3968, 1988),
    '../../objs/DC nObs_c.obj': (2, 3968, 1988),
    '../../objs/DC_nObs_c.obj': (2, 3968, 1988),
    '../../objs/DC nObs_e.obj': (2, 3968, 1988),
    '../../objs/DC nObs.obj': (2, 3968, 1988),
    '../../objs/DC pObs_c.obj': (2, 3968, 1988),
    '../../objs/DC pObs.obj': (2, 3968, 1988),
    '../../objs/EC cObs_c.obj': (2, 3968, 1988),
    '../../objs/EC cObs.obj': (2, 3968, 1988),
    '../../objs/EC eObs_c.obj': (2, 3968, 1988),
    '../../objs/EC eObs.obj': (2, 3968, 1988),
    '../../objs/EC nObs_c.obj': (2, 3968, 1988),
    '../../objs/EC nObs_e.obj': (2, 3968, 1988),
    '../../objs/EC nObs.obj': (2, 3968, 1988),
    '../../objs/EC pObs_c.obj': (2, 3968, 1988),
    '../../objs/EC pObs.obj': (2, 3968, 1988),
    '../../objs/EQ eObs_e.obj': (2, 3968, 1988),
    '../../objs/NTPPc cObs.obj': (2, 3968, 1988),
    '../../objs/NTPP cObs_c.obj': (2, 3968, 1988),
    '../../objs/PO cObs_c.obj': (2, 3968, 1988),
    '../../objs/PO cObs.obj': (2, 3968, 1988),
    '../../objs/PO eObs_c.obj': (2, 3968, 1988),
    '../../objs/PO eObs.obj': (2, 3968, 1988),
    '../../objs/PO pObs_c.obj': (2, 3968, 1988),
    '../../objs/PO pObs_e.obj': (2, 3968, 1988),
    '../../objs/PO pObs.obj': (2, 3968, 1988),
    '../../objs/TPPc cObs.obj': (2, 3968, 1988),
    '../../objs/TPPc eObs_e.obj': (2, 256, 132),
    '../../objs/TPPc eObs.obj': (2, 256, 132),
    '../../objs/TPP cObs.obj': (2, 3968, 1988),
    '../../objs/TPP eObs_c.obj': (2, 256, 132),
    '../../objs/TPP eObs_e.obj': (2, 256, 132)
    }


class TestObjLoader(unittest.TestCase):
    def test_file(self):
        for key in testingFiles:
            print "Loading " + key
            loader = Loader(key)
            #tests the number of polyhedrons
            assert len(loader.polyhedrons) == testingFiles[key][0]

            #tests the number of faces in the polyhedrons
            assert len(loader.polyhedrons[0].faceIndices()) + len(loader.polyhedrons[1].faceIndices()) == testingFiles[key][1]

            #tests the number of vertices in the polyhedrons
            assert len(loader.polyhedrons[0].vertices) + len(loader.polyhedrons[1].vertices) == testingFiles[key][2]

if __name__ == "__main__":
    unittest.main()
