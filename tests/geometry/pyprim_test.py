#
## Unit tests for the pure python geometry primative intersections methods
## Nathan Eloe and Ian Kottman
#

from numpy import array,copy,seterr, ndarray
from geometry import _pyprim

origin = array([0.,0.,0.])
centeredXYTri = array([[1.,-1.,0.],[-1.,-1.,0.],[0.,1.,0.]])

class TestPyprim(object):
    #point in triangle tests
    def pitIn1_test(self):
        assert _pyprim.pointInTri(origin,centeredXYTri)

    def pitOutOfPlane1_test(self):
        outofplane = copy(centeredXYTri)
        outofplane[0][2]=1.
        outofplane[1][2]=1.
        outofplane[2][2]=1.
        assert not _pyprim.pointInTri(origin,outofplane)

    def pitSamePlaneNotIn_test(self):
        pt = array([2.,0.,0.])
        assert not _pyprim.pointInTri(pt,centeredXYTri)

    def pitOnEdge_test(self):
        seterr(all='raise')
        pt = array([1.,-1.,0.])
        assert _pyprim.pointInTri(pt,centeredXYTri)

    #segment Triangle intersection tests
    def stContains_test(self):
        assert isinstance(_pyprim.segTriIntersect(origin,origin+.1,centeredXYTri), ndarray)

    def stOutside_test(self):
        assert not _pyprim.segTriIntersect(origin+2., origin+3., centeredXYTri)

    def stOneEndpointIn_test(self):
        assert isinstance(_pyprim.segTriIntersect(origin, origin+2., centeredXYTri), ndarray)

    def stIntersectNoEndptIn_test(self):
        assert isinstance(_pyprim.segTriIntersect(origin-3., origin+2., centeredXYTri), ndarray)

    def stOnEdge_test(self):
        assert isinstance(_pyprim.segTriIntersect(array([-1.,-1.,0.]), array([0.,1.,0.]),
                centeredXYTri), ndarray)

    def stParallelOutOfPlane_test(self):
        assert not _pyprim.segTriIntersect(array([-1.,0.,5.]),array([1.,0.,5.]),
                centeredXYTri)

    def rtContains_test(self):
        assert isinstance(_pyprim.rayTriIntersect(origin,origin+.1,centeredXYTri), ndarray)

    def rtOutsideAway_test(self):
        #print _pyprim.rayTriIntersect(origin+2., origin+3., centeredXYTri)
        assert not _pyprim.rayTriIntersect(origin+2., origin+3., centeredXYTri)

    def rtOutsideTo_test(self):
        assert isinstance(_pyprim.rayTriIntersect(origin+3., origin+2., centeredXYTri), ndarray)

    def rtOneEndpointIn_test(self):
        assert isinstance(_pyprim.rayTriIntersect(origin, origin+2., centeredXYTri), ndarray)

    def rtIntersectNoEndptIn_test(self):
        assert isinstance(_pyprim.rayTriIntersect(origin-3., origin+2., centeredXYTri), ndarray)

    def rtOnEdge_test(self):
        #print 'rtOnEdge'
        assert isinstance(_pyprim.rayTriIntersect(array([-1.,-1.,0.]), array([0.,1.,0.]),
                centeredXYTri), ndarray)

    def rtParallelOutOfPlane_test(self):
        assert not _pyprim.rayTriIntersect(array([-1.,0.,5.]),array([1.,0.,5.]),
                centeredXYTri)
