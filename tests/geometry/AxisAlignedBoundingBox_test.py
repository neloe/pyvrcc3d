from geometry import AxisAlignedBoundingBox as aabb
from numpy import array

class TestAxisAlignedBoundingBox(object):

    #tests intersection when both aabbs are the same
    def aabbIntersect_same_test(self):
        first = aabb.AxisAlignedBoundingBox()
        second = aabb.AxisAlignedBoundingBox()
        first.ubound = array([1.0, 1.0, 1.0])
        second.ubound = array([1.0, 1.0, 1.0])
        assert first.aabbIntersect(second)

    #tests intersection when the aabbs do not intersect
    def aabbIntersect_not_intersect_test(self):
        first = aabb.AxisAlignedBoundingBox()
        second = aabb.AxisAlignedBoundingBox()
        first.ubound = array([4.0, 4.0, 4.0])
        second.lbound = array([0.0, 0.0, -5.0])
        second.ubound = array([4.0, 4.0, -1.0])
        assert not first.aabbIntersect(second)

    #tests intersection when the aabbs intersect on a line
    def aabbIntersect_intersect_line_test(self):
        first = aabb.AxisAlignedBoundingBox()
        second = aabb.AxisAlignedBoundingBox()
        first.ubound = array([4.0, 4.0, 4.0])
        second.lbound = array([0.0, 4.0, -4.0])
        second.ubound = array([4.0, 8.0, 0.0])
        assert first.aabbIntersect(second)

    #tests intersection when the aabbs intersect at a single point
    def aabbIntersect_intersect_point_test(self):
        first = aabb.AxisAlignedBoundingBox()
        second = aabb.AxisAlignedBoundingBox()
        first.ubound = array([4.0, 4.0, 4.0])
        second.lbound = array([4.0, 4.0, -4.0])
        second.ubound = array([8.0, 8.0, 0.0])
        assert first.aabbIntersect(second)

    #tests intersection when the aabbs intersect through the middle, like a cross
    def aabbIntersect_intersect_line_test(self):
        first = aabb.AxisAlignedBoundingBox()
        second = aabb.AxisAlignedBoundingBox()
        first.ubound = array([4.0, 4.0, 4.0])
        second.lbound = array([1.0, 1.0, -1.0])
        second.ubound = array([3.0, 3.0, 5.0])
        assert first.aabbIntersect(second)
