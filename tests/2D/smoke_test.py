# @file	smoke_test.py
# @desc Run some smoke tests on loading some of the sample files to verify
#        they load properly
# @auth Doug McGeehan (djmvfb@mst.edu)

import os
import sys

# Add the PyVRCC-3D root to the system path so that imports can be made
pyvrcc3d_root = os.path.abspath(os.path.join(os.path.dirname(
                 os.path.abspath(__file__)), "..", ".."))
sys.path.append(pyvrcc3d_root)

from geometry.Polyhedron import Polyhedron
import numpy

def load_dummy_polyhedron_test():
  dummy_name = "dummy"
  simple_triangle_on_xy_plane = numpy.array([[0,0,0], [1,0,0], [0,1,0]])
  triangle_face_indices = numpy.array([0, 1, 2])
  dummy_polyhedron = Polyhedron(name=dummy_name,
    verts=simple_triangle_on_xy_plane, indices=triangle_face_indices)
  
  assert(dummy_name == dummy_polyhedron.name)
  for point in simple_triangle_on_xy_plane:
    assert(point in dummy_polyhedron.vertices)
  for index in triangle_face_indices:
    assert(index in dummy_polyhedron.indices)

if __name__ == "__main__":
  load_dummy_polyhedron_test()
