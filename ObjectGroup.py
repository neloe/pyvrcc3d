# ObjectGroup.py
# Holds a group of Objects with something
# Nathan Eloe


from Object import Object
from numpy import array
from OpenGL.GL import *
from OpenGL.arrays import vbo
from OpenGL.GL.shaders import *

class ObjectGroup:
    def __init__(self):
        self.objects=list()
        self.shaders=list()
    def render(self):
        #print 'ObjectGroup render()',len(self.objects)
	[o.render() for o in self.objects]
        #[(glUseProgram(s),
        #  [o.render(True) for o in self.objects if o.shader==s],
        # glUseProgram(0)) for s in self.shaders]


