#
# Pure python geometry primative intersection methods
# Nathan Eloe
#

import numpy
from math import copysign
import hashlib

_normcache = {}


def norm(tri):
    # return _normcache.setdefault(hashlib.sha1(tri).hexdigest(), numpy.cross(tri[2] - tri[0], tri[1] - tri[0]))
    # print tri
    return _normcache.setdefault(hashlib.sha1(tri).hexdigest(), numpy.cross(*((tri - tri[0])[1:])))
    # return c / numpy.sqrt(c.dot(c))


def pointInTri(I, tri):
    v = numpy.array([tri[2] - tri[0], tri[1] - tri[0], I - tri[0]])
    dot0 = [numpy.dot(v[0], i) for i in v]
    dot1 = [numpy.dot(v[1], i) for i in v[1:]]
    D = 1. / (dot0[0] * dot1[0] - dot0[1] * dot0[1])
    u = (dot1[0] * dot0[2] - dot0[1] * dot1[1]) * D
    v = (dot0[0] * dot1[1] - dot0[1] * dot0[2]) * D

    return u >= 0 and v >= 0 and u + v <= 1


def lineLineParInt(a0, a1, b0, b1, eps=None):
    A = a1 - a0
    B = b0 - b1
    t, s = numpy.linalg.lstsq(numpy.array([A, B]).T, b0 - a0)[0]
    pa = t * A + a0
    pb = s * B + b0
    err = 0.5 * (eps(pa) + eps(pb)) if eps else 0.0
    if numpy.dot(pa - pb, pa - pb) <= err:
        return t, s
    return False


def rayTriIntersect(r0, r1, tri, eps=None):
    '''Returns the point of intersection on the ray if intersection occurs,
    else, returns false'''
    n = norm(tri)
    dR = r1 - r0
    denom = numpy.dot(dR, n)
    if denom == 0:
        if pointInTri(r0, tri):
            return r0
        elif pointInTri(r1, tri):
            return r1
        # handle case where ray in plane, and may pass through the triangle
        st = lineLineParInt(r0, r1, tri[0], tri[1], eps)
        if not isinstance(st, bool) and 0.0 <= st[1] <= 1.0 and st[0] >= 0.0:
            return st[0] * dR + r0
        st = lineLineParInt(r0, r1, tri[1], tri[2], eps)
        if not isinstance(st, bool) and 0.0 <= st[1] <= 1.0 and st[0] >= 0.0:
            return st[0] * dR + r0
        st = lineLineParInt(r0, r1, tri[0], tri[2], eps)
        if not isinstance(st, bool) and 0.0 <= st[1] <= 1.0 and st[0] >= 0.0:
            return st[0] * dR + r0
        return False
    t = numpy.dot(tri[0] - r0, n) / denom
    pt = t * dR + r0
    if t < 0.0 or not pointInTri(pt, tri):
        return False
    return pt


def segTriIntersect(r0, r1, tri, eps=None):
    '''Returns the point of intersection on the ray if intersection occurs,
    else, returns false'''
    n = norm(tri)
    #d = numpy.dot(tri[0], n)
    dR = r1 - r0
    denom = numpy.dot(dR, n)
    if denom == 0:
        if pointInTri(r0, tri):
            return r0
        elif pointInTri(r1, tri):
            return r1
        # handle case where ray in plane, and may pass through the triangle
        st = lineLineParInt(r0, r1, tri[0], tri[1], eps)
        if not isinstance(st, bool) and 0.0 <= st[1] <= 1.0 and 1.0 >= st[0] >= 0.0:
            return st[0] * dR + r0
        st = lineLineParInt(r0, r1, tri[1], tri[2], eps)
        if not isinstance(st, bool) and 0.0 <= st[1] <= 1.0 and 1.0 >= st[0] >= 0.0:
            return st[0] * dR + r0
        st = lineLineParInt(r0, r1, tri[0], tri[2], eps)
        if not isinstance(st, bool) and 0.0 <= st[1] <= 1.0 and 1.0 >= st[0] >= 0.0:
            return st[0] * dR + r0
        return False
    t = numpy.dot(tri[0] - r0, n) / denom
    #t = numpy.dot(dR-r0, n) / denom
    # print t, t * dR + r0, r0, r1
    if t < 0.0 or t > 1.0:
        return False
    return t * dR + r0


def triTriIntersect(tri1, tri2, eps=None):
    uv = (tri1 - tri1[0])[1:]
    n1 = norm(tri1)
    n2 = norm(tri2)
    un = numpy.dot(uv[0], n2)
    vn = numpy.dot(uv[1], n2)
    if un == 0 and vn == 0:
        #parallel intersection
        return segTriIntersect(tri1[0], tri1[1], tri2) is not False and\
            segTriIntersect(tri1[0], tri1[2], tri2) is not False and\
            segTriIntersect(tri1[1], tri1[2], tri2) is not False
    denom = 1.0 / (un * un + vn * vn)
    lambdam = float('-inf')
    lambdaM = float('inf')
    ualpha = numpy.dot(tri2[0]-tri1[0], n2) * un * denom
    valpha = numpy.dot(tri2[0]-tri1[0], n2) * vn * denom
    if vn != 0:
        lrange = sorted([ualpha / vn, (1-ualpha) / vn])
        lambdam = max(lambdam, lrange[0])
        lambdaM = min(lambdaM, lrange[1])
    if lambdaM < lambdam:
        return False
    if un != 0:
        lrange = sorted([valpha / un, (valpha - 1) / un])
        lambdam = max(lambdam, lrange[0])
        lambdaM = min(lambdaM, lrange[1])
    if lambdaM < lambdam:
        return False
    if vn - un != 0:
        lrange = sorted([(ualpha + valpha) / (vn - un), (1 - (ualpha + valpha)) / (vn - un)])
        lambdam = max(lambdam, lrange[0])
        lambdaM = min(lambdaM, lrange[1])
    return lambdaM >= lambdam

'''def triTriIntersect(tri1, tri2, eps=None):
    return (not isinstance(segTriIntersect(tri1[0], tri1[1], tri2, eps), bool)) or\
        (not isinstance(segTriIntersect(tri1[1], tri1[2], tri2, eps), bool)) or\
        (not isinstance(segTriIntersect(tri1[0], tri1[2], tri2, eps), bool)) or\
        (not isinstance(segTriIntersect(tri2[0], tri2[1], tri1, eps), bool)) or\
        (not isinstance(segTriIntersect(tri2[1], tri2[2], tri1, eps), bool)) or\
        (not isinstance(
            segTriIntersect(tri2[0], tri2[2], tri1, eps), bool))
'''

def normConflicts(vec, norm):
    return any(vec * norm <= 0.)
