#
# Unit tests for the pure python geometry primative intersections methods
# Nathan Eloe
#

from numpy import array, copy, seterr
import _pyprim

origin = array([0., 0., 0.])
centeredXYTri = array([[1., -1., 0.], [-1., -1., 0.], [0., 1., 0.]])

# point in triangle tests


def pitIn1_test():
    return _pyprim.pointInTri(origin, centeredXYTri)


def pitOutOfPlane1_test():
    outofplane = copy(centeredXYTri)
    outofplane[0][2] = 1.
    outofplane[1][2] = 1.
    outofplane[2][2] = 1.
    return not _pyprim.pointInTri(origin, outofplane)


def pitSamePlaneNotIn_test():
    pt = array([2., 0., 0.])
    return not _pyprim.pointInTri(pt, centeredXYTri)


def pitOnEdge_test():
    seterr(all='raise')
    pt = array([1., -1., 0.])
    return _pyprim.pointInTri(pt, centeredXYTri)

# segment Triangle intersection tests


def stContains_test():
    return _pyprim.segTriIntersect(origin, origin + .1, centeredXYTri)


def stOutside_test():
    return not _pyprim.segTriIntersect(origin + 2., origin + 3., centeredXYTri)


def stOneEndpointIn_test():
    return _pyprim.segTriIntersect(origin, origin + 2., centeredXYTri)


def stIntersectNoEndptIn_test():
    return _pyprim.segTriIntersect(origin - 3., origin + 2., centeredXYTri)


def stOnEdge_test():
    return _pyprim.segTriIntersect(array([-1., -1., 0.]), array([0., 1., 0.]),
                                   centeredXYTri)


def stParallelOutOfPlane_test():
    return _pyprim.segTriIntersect(array([-1., 0., 5.]), array([1., 0., 5.]),
                                   centeredXYTri)
