#
# \file Polyhedron.py
# \brief a class to handle polyhedrons and maths on said polyhedrons
#
import numpy
from AABBTree import AABBTree
from AABBTreeV2 import AABBTreeV2
from AxisAlignedBoundingBox import AxisAlignedBoundingBox
from _pyprim import triTriIntersect, segTriIntersect, normConflicts, norm, rayTriIntersect, pointInTri
from numpy import dot
from Polygon import Polygon
import sys
sys.path.append('..')
from viewport import CAM_POS, iPlane
import hashlib
import pprint
from itertools import chain


class Polyhedron:

    def __init__(self, name, verts, indices):
        '''Initialize the polyhedron to a set of set of vertices and indices

        :param name: the name of the parameter
        :param verts: the numpy array of 3d points
        :param indices: the indices into the vertex array, defines the faces
        :type name: str
        :type verts: numpy.array
        :type indices: numpy.array
        '''
        # Some useful lambdas, so I don't have to define them every time
        # Get the magnitude squared of a vector
        self.norm2sq = lambda x: numpy.dot(x, x)
        # Some useful "constants
        self.IN = -1
        self.ON = 0
        self.OUT = 1
        # basic information
        self.name = name
        self.vertices = verts
        self.indices = indices
        # calculate the normals
        # information about the spin, orbit around the origin, and translation
        self._orbit = numpy.array([0., 0., 0.])
        self._spin = numpy.array([0., 0., 0.])
        # translate my faces to the origin, and track their translation
        self._translation = self.centerOfMass()
        self.vertices -= self._translation
        # Method borrowed from glumpy examples, don't ask me how it works
        self._updateNormals()
        self._projectionChanged = True
        self.perspective_projection = Polygon()
        self._updateProjection()
        #self.aabbTree = AABBTree(self.faces())
        #self.newTree = AABBTreeV2(self.faces())
        self.aabbTree = AABBTreeV2(self.faces())

    def _updateNormals(self):
        '''Updates the store of normal values
        Method borrowed from glumpy examples
        '''
        T = self.faces()
        N = numpy.cross(T[::, 1] - T[::, 0], T[::, 2] - T[::, 0])
        L = numpy.sqrt(N[:, 0] ** 2 + N[:, 1] ** 2 + N[:, 2] ** 2)
        N /= L[:, numpy.newaxis]
        self.normals = numpy.zeros(self.vertices.shape)
        self.normals[self.indices.reshape(-1, 3)[:, 0]] += N
        self.normals[self.indices.reshape(-1, 3)[:, 1]] += N
        self.normals[self.indices.reshape(-1, 3)[:, 2]] += N
        L = numpy.sqrt(self.normals[:, 0] ** 2 + self.normals[
                       :, 1] ** 2 + self.normals[:, 2] ** 2)
        self.normals /= L[:, numpy.newaxis]

    def faces(self):
        '''Returns a list of the vertices that make the faces
        :return: numpy.array of faces
        '''
        return self.transformVerts()[self.faceIndices()]

    def faceIndices(self):
        '''Returns a list of the indices in the correct shape to be faces
        :return: numpy.array of indices
        '''
        return self.indices.reshape(-1, 3)

    def centerOfMass(self):
        return sum(self.vertices) / len(self.vertices)

    def _updateProjection(self):
        '''Updates the stored projection.  Slow calculation so don't call this from outside the class
        '''
        #visible = self.normals[:, 0] >= 0.0
        projectedVerts = self.transformVerts().copy()
        map(self.normalizeZ, projectedVerts)
        projectedFaces = numpy.array(projectedVerts)[
            self.indices.reshape(-1, 3)][:, :, :2].reshape(-1, 3, 2)
        self.perspective_projection = Polygon()

        def updateAndSimplify(c):
            self.perspective_projection.addContour(c)
            self.perspective_projection.simplify()

        map(updateAndSimplify, projectedFaces)
        self._projectionChanged = False

    def transformVerts(self):
        '''Applies the transformations stored to the points.  Does not modify points
        For use in calculations only, not drawing'''
        return numpy.dot(numpy.dot(self.vertices, self._spinMatrix()) + self._translation, self._orbitMatrix())

    def _spinMatrix(self):
        return self._rotationMatrix(self._spin)

    def _rotationMatrix(self, rotation):
        '''Generates the rotation matrix for the supplied set of rotation
        :param rotation: the rotation to generate the rotation matrix for
        :type rotation: numpy.array (1x3)
        :return: numpy.array (3x3)'''
        sins = numpy.sin(rotation)
        coss = numpy.cos(rotation)
        # from the almighty redbook!
        rotationMatrix = [
            [coss[1] * coss[0], sins[2] * sins[1] * coss[0] - coss[1] * sins[0],
             sins[1] * sins[0] + coss[2] * sins[1] * coss[0]],
            [coss[1] * sins[0], coss[2] * coss[0] + sins[2] * sins[1] * sins[0],
             coss[2] * sins[1] * sins[0] - sins[2] * coss[0]],
            [-1 * sins[1], sins[2] * coss[1], coss[2] * coss[2]]]
        return numpy.array(rotationMatrix)

    def _orbitMatrix(self):
        return self._rotationMatrix(self._orbit)

    def normalizeZ(self, pt):
        pt /= iPlane(pt)

    def projection(self):
        '''Updates the stored projection, if necessary, and returns the projection
        '''
        if self._projectionChanged:
            self._updateProjection(self)
        return self.perspective_projection

    def aggProj(self, p):
        '''this should REALLY just be able to be a cascaded union, can we fix that please?'''
        try:
            self.perspective_projection = self.perspective_projection.union(p)
        except:
            pass

    # Transformation and translation functions
    def translate(self, t):
        '''Adds the scalar or vector translation to the overall translation
        :param t: the change in position
        :type t: scalar or vector
        '''
        self._translation += t
        self._projectionChanged = True

    def orbit(self, o):
        '''Adds the scalar or vector orbit to the overall orbit around the origin
        :param o: the change in position
        :type o: scalar or vector
        '''
        self._orbit += o
        self._projectionChanged = True

    def spin(self, s):
        '''Adds the scalar or vector spin to the overall spin about the center of mass
        :param s: the change in position
        :type s: scalar or vector
        '''
        self._spin += s
        self._projectionChanged = True

    def getTranslation(self):
        return self._translation

    def getOrbitAngle(self):
        return self._orbit

    def getSpinAngle(self):
        return self._spin

    # some more primative logicy bits
    # these will need to be rewritten since possibleSegIntersections doesn't exist
    # also, return representation changes

    def classifyPoint(self, point, eps=lambda x: 0.0):
        midpt = sum(self.faces()[0]) / 3.0
        #which bounding box should we be checking this against, root?
        possible = self.aabbTree.RayBoxIntersection((point, midpt))
        #ints = filter(lambda x: not isinstance(x[1], bool),
        #              chain(*[[(p, rayTriIntersect(point, midpt, b), b) for b in self.aabbTree.tree[p]
        #                      if not isinstance(b, AxisAlignedBoundingBox)]for p in possible]))
        #chain(*[[(box, rayTriIntersect(point, midpt, face), face) for face in self.aabbTree.getFaces(box)] for box in possible])
        #print AxisAlignedBoundingBox(self.faces()[0])
        ints = filter(lambda x: not isinstance(x[1], bool),
                      [(face, rayTriIntersect(point, midpt, face)) for face in self.aabbTree.getFaces(*possible)])
        #print point, midpt
        cface, endpt = min(ints, key=lambda x: self.norm2sq(x[1] - point))
        err = eps(point)
        seg = endpt - point
        if self.norm2sq(seg) < err * err:
            return self.ON
        return [self.OUT, self.IN][numpy.dot(norm(cface), seg) > 0.0]

    def pointInside(self, point, eps=lambda x: 0.0):
        return self.classifyPoint(point, eps) == self.IN

    def pointOutside(self, point, eps=lambda x: 0.0):
        return self.classifyPoint(point, eps) == self.OUT

    # The needed intersection algorithms for VRCC-3D+
    def bndBnd(self, otherPoly, eps=None):
        #possible = self.aabbTree.possibleIntersections(otherPoly.aabbTree)
        possible = self.aabbTree.BoxBoxIntersection(
            self.aabbTree.root, otherPoly.aabbTree, otherPoly.aabbTree.root)
        for aabb in possible:
            faces = self.aabbTree.getFaces(aabb)
            for face in faces:
                otherFaces = otherPoly.aabbTree.getFaces(*possible[aabb])
                #print [triTriIntersect(face, o, eps) for o in otherFaces if not isinstance(o, AxisAlignedBoundingBox)]
                if any([triTriIntersect(face, o, eps) for o in otherFaces if not isinstance(o, AxisAlignedBoundingBox)]):
                    return True
        '''for aabb in possible:
            faces = self.aabbTree.aabbTree.tree[aabb]
            for face in faces:
                if any([triTriIntersect(face, otherPoly.aabbTree.aabbtofaces[box],
                                    eps) for box in possible[aabb]]):
                    return True'''
        return False

    def bndInt(self, otherPoly, eps=lambda x: 0.0):
        #possible = self.aabbTree.possibleIntersections(otherPoly.aabbTree)
        possible = self.aabbTree.BoxBoxIntersection(
            self.aabbTree.root, otherPoly.aabbTree, otherPoly.aabbTree.root)
        if len(possible.keys()) == 0:
            #print 'no possible intersections'
            return otherPoly.pointInside(self.transformVerts()[0], eps)
        #print len(possible.keys()), 'possible intersections'
        for aabb in possible:
            faces = self.aabbTree.tree[aabb]
            for face in faces:
                if not isinstance(face, AxisAlignedBoundingBox) and any([otherPoly.pointInside(p, eps) for p in face]):
                    return True
        return False

    def intBnd(self, otherPoly, eps=lambda x: 0.0):
        return otherPoly.bndInt(self)

    def bndExt(self, otherPoly, eps=lambda x: 0.0):
        #possible = self.aabbTree.possibleIntersections(otherPoly.aabbTree)
        possible = self.aabbTree.BoxBoxIntersection(
            self.aabbTree.root, otherPoly.aabbTree, otherPoly.aabbTree.root)
        # print 'bndExt', len(possible), self.transformVerts()[0], otherPoly._translation
        # print self.name, sum(self.transformVerts()) / len(self.transformVerts())
        # print otherPoly.name, sum(otherPoly.transformVerts()) / len(otherPoly.transformVerts())
        # print '---'
        # print 'possible has',len(possible.keys()),'keys'
        if len(possible.keys()) == 0:
            # print 'point comparing:',self.transformVerts()[0]
            # print 'otherPoly',otherPoly.faces()
            return otherPoly.pointOutside(self.transformVerts()[0], eps)
        #print 'bndExt;',len(possible.keys()), 'possible intersections'
        for aabb in possible:
            faces = self.aabbTree.tree[aabb]
            for face in faces:
                if not isinstance(face, AxisAlignedBoundingBox) and any([otherPoly.pointOutside(p, eps) for p in face]):
                    return True
        #print 'all done!'
        return False

    def extBnd(self, otherPoly, eps=lambda x: 0.0):
        return otherPoly.bndExt(self, eps)
