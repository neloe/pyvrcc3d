#
# unproject.py
# Nathan Eloe
# A function to unproject an image point to the nearest point on the object
#


import numpy
import sys
if '..' not in sys.path:
    sys.path.append('..')
from viewport import NEAR, FAR, CAM_POS, eps
from _pyprim import rayTriIntersect
from itertools import chain
from AxisAlignedBoundingBox import AxisAlignedBoundingBox

# everytime proj in polyhedron... clear cache?
# remove cache at polyname
_cache = {}


def unproject(poly, x, y):
    global _cache
    if poly.name not in _cache:
        _cache[poly.name] = {}
    if (x, y) not in _cache[poly.name]:
        farpt = numpy.append((x, y), [CAM_POS[2] - NEAR])
        possible = poly.aabbTree.RayBoxIntersection((CAM_POS, farpt))
        st = lambda t: rayTriIntersect(CAM_POS, farpt, t, eps)
        faces = [[f for f in poly.aabbTree.tree[b] if not isinstance(f, AxisAlignedBoundingBox)] for b in possible]
        unproj = filter(lambda x: not isinstance(x, bool), map(st, chain(*faces)))
        if len(unproj) == 0:
            _cache[poly.name][(x, y)] = False
        else:
            _cache[poly.name][(x, y)] = max(unproj, key=lambda x: x[2])
    return _cache[poly.name][(x, y)]
