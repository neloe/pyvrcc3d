#
# \file AABBTree.py
# \brief An Axis Aligned Bounding Box Intersection Tree
# Used to filter out faces that cannot intersect
#
from AxisAlignedBoundingBox import AxisAlignedBoundingBox
from utils import pairwise
from math import sqrt
from numpy import dot, array


class AABBTree:

    def __init__(self, faces):
        '''Creates an Axis Aligned Bounding Box Tree used to filter out impossible intersection choices
        faces should be the actual faces of points, not indices
        '''

        self.aabbtofaces = {
            AxisAlignedBoundingBox(face): face for face in faces}
        self.tree = self.aabbtofaces.keys()
        self.tree.sort()

# could this be a list comprehension? would it be faster? -nwe
        for element1, element2 in pairwise(self.tree):
            # passes in points from the 2 bounding boxes coming together rather than face points
            # Epic misuse of AABB constructor
            self.tree.append(AxisAlignedBoundingBox(
                [element1.uBound, element1.lBound, element2.uBound, element2.lBound]))
            # If there is only one element at the end of the list, we just made the root of the tree
            # We don't want to keep going, so break the eff out of here.

        # This puts the root of the tree at the front.
        self.tree.reverse()
        self.numfaces = len(faces)
        self.intersects = {}

    def possibleIntersections(self, otherTree):
        self._intersections = {}
        if self.tree[0].aabbIntersect(otherTree.tree[0]):
            self._checkIntersections(0, otherTree, 0)
#myboxes = self.aabbtofaces.keys, otherb = other.aabbtofaces.keys()

# return {box:[b for b in otherB if box.intersect(b)] for box in myboxes}
        return self._intersections

    def _checkIntersections(self, index, otherTree, otherIndex):
        if(self.tree[index].aabbIntersect(otherTree.tree[otherIndex])):
            these_children = self.getChildrenIndices(index)
            other_children = otherTree.getChildrenIndices(otherIndex)

            if these_children != [] and other_children != []:
                self._checkIntersections(
                    these_children[0], otherTree, other_children[0])
                self._checkIntersections(
                    these_children[0], otherTree, other_children[1])
                self._checkIntersections(
                    these_children[1], otherTree, other_children[0])
                self._checkIntersections(
                    these_children[1], otherTree, other_children[1])
            elif these_children == [] and other_children != []:
                self._checkIntersections(index, otherTree, other_children[0])
                self._checkIntersections(index, otherTree, other_children[1])
            elif these_children != [] and other_children == []:
                self._checkIntersections(
                    these_children[0], otherTree, otherIndex)
                self._checkIntersections(
                    these_children[1], otherTree, otherIndex)
            else:
                if self.tree[index] not in self._intersections:
                    self._intersections[self.tree[index]] = []
                self._intersections[self.tree[index]].append(
                    otherTree.tree[otherIndex])

    def possibleSegIntersections(self, start, end, index=0):
        pos = []
        val = self.tree[index].segmentIntersect((start, end))
        print 'segment:', (start, end), 'Index:', index, 'Box:', self.tree[index], 'Intersect:', val
        if self.tree[index].segmentIntersect((start, end)):
            if self.tree[index] in self.aabbtofaces:
                pos.append(self.aabbtofaces)
            else:
                pos[-1:] = self.possibleSegIntersections(
                    start, end, 2 * index + 1)
                pos[-1:] = self.possibleSegIntersections(
                    start, end, 2 * index + 2)
        return pos

    def possibleRayIntersections(self, r0, r1, index=0):
        '''
        pos = []
        #print 'in ray intersections aabb tree'
        val = self.tree[index].rayIntersect((r0, r1))
        #print 'ray:', (r0, r1), 'Index:', index, 'Box:', self.tree[index], 'Intersect:', val
        if val:
            if self.tree[index] in self.aabbtofaces:
                pos.append(self.aabbtofaces[self.tree[index]])
            else:
                pos.extend(self.possibleRayIntersections(r0, r1, 2 * index + 1))
                pos.extend(self.possibleRayIntersections(r0, r1, 2 * index + 2))
        return pos
        '''
        return [self.aabbtofaces[box] for box in self.aabbtofaces if box.rayIntersect((r0, r1))]

    # Returns indices of children of node at index
    def getChildrenIndices(self, index):
        return filter(lambda x: x < len(self.tree),
                      [2 * index + 1, 2 * index + 2])

    # Returns all nodes at a given depth
    def getChildrenAtDepth(self, depth):
        if depth == 0:
            return self.tree[0]
        startposition = 2 ** depth - 1
        number = 2 ** depth

        return self.tree[startposition:(startposition + number)]
