#
# AxisAlignedBoundingBox.py
# Programmer: Nathan Eloe
# An implementation of an axis aligned bounding box
#
import numpy
import hashlib


class AxisAlignedBoundingBox:

    def __init__(self, points=None):
        self.lBound = numpy.array([0.0, 0.0, 0.0])
        self.uBound = numpy.array([0.0, 0.0, 0.0])
        self.intersectionMethods = {'point': self.inBox,
                                    'aabb': self.aabbIntersect}
        if points is not None:
            self.lBound = numpy.amin(points, 0)
            self.uBound = numpy.amax(points, 0)

        self.id = hashlib.sha1(
            numpy.append(self.lBound, self.uBound)).hexdigest()

    def __hash__(self):
        return int(self.id, 16)

    def __lt__(self, other):
        lessthan = numpy.append(self.lBound < other.lBound, [True])
        greaterthan = numpy.append(self.lBound > other.lBound, [True])
        return lessthan.argmax() < greaterthan.argmax()

    def __eq__(self, other):
        return all(self.lBound == other.lBound) and all(self.uBound == other.uBound)

    def __str__(self):
        return 'UBound: ' + self.uBound.__str__() + '; LBound: ' + self.lBound.__str__()

    def center(self):
        return (self.lBound + self.uBound) / 2.0

    def inBox(self, point):
        return all(self.lBound <= point) and all(point <= self.uBound)

    def aabbIntersect(self, box, eps=lambda x: .00001):
        '''Determines whether another AABB intersects this box

        :param box: AABB to check intersection with
        :type box: AxisAlignedBoundingBox
        :return bool: True if box intersects, else False
        '''
        return all(self.uBound - box.lBound >= -eps(self.uBound)) and all(self.lBound - box.uBound <= eps(self.uBound))

    def segmentIntersect(self, segment):
        '''Determines whether a line segment intersects this box

        :param segment: The segment to check for intersection with
        :type segment: tuple (start_pt, end_pt), start_pt and end_pt are numpy arrays
        :return bool: True if segment intersects, else False
        '''
        # First, see if either of the points are in the box itself
        if self.inBox(segment[0]) or self.inBox(segment[1]):
            return True
        # If both endpoints are not in the box, we need to find if we intersect any planes
        # We will intersect either 2 or 4 planes (if the point of intersection is on a corner)
        # We only need to find one
        slope = segment[1] - segment[0]

        def checkAxisPlane(axis):
            if slope[axis] == 0.0:
                print 'axis slope 0:', axis, self.uBound, self.lBound, segment[0], segment[1]
                mask = numpy.array([True, True, True])
                mask[axis] = False
                print self.uBound[mask] >= segment[0][mask], self.lBound[mask] <= segment[1][mask]
                return all(self.uBound[mask] >= segment[0][mask]) and all(self.lBound[mask] <= segment[1][mask])
            # delta = lBound[axis]slope[axis]
            srec = 1.0 / slope[axis]
            delta = (self.lBound[axis] - segment[0][axis]) * srec
            planeIntersect = slope * delta + segment[0]
            ok = (self.lBound >= planeIntersect) * \
                (planeIntersect <= self.uBound)
            if all(ok):
                return True
            delta = (self.uBound[axis] - segment[0][axis]) * srec
            planeIntersect = slope * delta + segment[0]
            ok = (self.lBound >= planeIntersect) * \
                (planeIntersect <= self.uBound)
            return all(ok)
        return any(map(checkAxisPlane, range(3)))

    def rayIntersect(self, ray):
        '''Determines whether a line segment intersects this box

        :param ray: The ray to check for intersection with
        :type ray: tuple (start_pt, end_pt), start_pt and end_pt are numpy arrays
        :return bool: True if segment intersects, else False
        '''
        # see if the starting point is in the box
        dR = ray[1] - ray[0]
        ts = (self.lBound[dR != 0] - ray[0][dR != 0]) / dR[dR != 0]
        for t in ts:
            if t >= 0:
                coord = t * dR + ray[0]
                if all(((coord - self.lBound) > -.00001) & ((self.uBound - coord) > -.00001)):
                    return True
        ts = (self.uBound[dR != 0] - ray[0][dR != 0]) / dR[dR != 0]
        for t in ts:
            if t >= 0:
                coord = t * dR + ray[0]
                if all(((coord - self.lBound) > -.00001) & ((self.uBound - coord) > -.00001)):
                    return True
        return False
        '''
        lt = ray[0] < self.lBound
        gt = ray[0] > self.uBound
        if not any(lt | gt):
            return True
        #coord = self.lBound.copy()
        #coord[gt] = self.uBound[gt]
        maxT = numpy.array([-1., -1., -1.])
        maxT[lt] = (self.lBound - ray[0])[lt]
        maxT[gt] = (self.uBound - ray[0])[gt]
        maxT[dR != 0.0] /= dR[dR != 0.0]
        wp = numpy.argmax(maxT)
        # print wp, (lt | gt)
        # if not (lt | gt)[wp]:
        #    print 'not (lt|gt)[wp] is True'
        if maxT[wp] == -1.0:
            # print 'maxT[wp] == -1.0'
            return False
        mask = [False, False, False]
        mask[wp] = False
        coord = ray[0] + maxT[wp] * dR
        return all(coord[mask] > self.lBound[mask]) or all(coord[mask] < self.uBound[mask])
        '''
        '''
        dRay = ray[1] - ray[0]

        def checkSide(side):
            if dRay[side] == 0.0:
                mask = numpy.array([True, True, True])
                mask[side] = False
                return all(self.uBound[mask] >= ray[0][mask]) and all(self.lBound[mask] <= ray[1][mask])
            t = (self.lBound[side] - ray[0][side]) / dRay[side]
            isect = t * dRay + ray[0]
            if all((isect >= self.lBound) * (isect <= self.uBound)):
                return True
            t = (self.uBound[side] - ray[0][side]) / dRay[side]
            isect = t * dRay + ray[0]
            return all((isect >= self.lBound) * (isect <= self.uBound))

        return any(map(checkSide, range(3)))
        '''

    def intersection(self, shape, method='aabb'):
        if method.lower() not in self.intersectionMethods:
            return self.intersectionMethods[method.lower()](shape)
        raise NotImplementedError('Invalid intersection method. \
                                  Implemented methods are: ' +
                                  self.intersectionMethods.keys())
