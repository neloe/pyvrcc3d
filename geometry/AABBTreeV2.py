from AxisAlignedBoundingBox import AxisAlignedBoundingBox
from utils import pairwise
from math import sqrt, floor, ceil
import numpy as np
from collections import defaultdict
from itertools import chain


class AABBTreeV2:

    def __init__(self, faces):
        #self.adj = dict()  # adjacency matrix (dictionary)
        self.adj = defaultdict(list)
        self.tree = dict()
        self.root = None  # the root of the tree, a bounding box
        #self.aabbtofaces = {AxisAlignedBoundingBox(face): face for face in faces}
        # 'counted faces' maps a number n to a reference to a face
        self.cfaces = dict(enumerate(faces))

        # this code builds an adj mat. it is much cleaner, but significantly slower than the procedural stuff that we currently use
        # ptin = lambda pt,face: any(map(lambda x: (pt==x).all(), face)) #all is pretty expensive. how could we make it chaper
        #fint = lambda f1, f2: sum(map(lambda x: ptin(x, f2), f1)) == 2
        #self.adj = dict(enumerate([[i[0] for i in j] for j in [filter(lambda x: fint(f,x[1]), enumerate(faces)) for f in faces]]))

        # tests if two points a,b are equal by comparing their x,y,z
        # coordinates
        def pteq(a, b):
            return (a[0] == b[0]) and (a[1] == b[1]) and (a[2] == b[2])

        # a1 is a face
        # a2 is a face
        def ct_shrd_pts(a1, a2):
            ct = 0  # assume the two faces share zero points
            for a in a2: # for point in face
                # if the first pt in a1 == any point a in a2
                if pteq(a1[0], a):
                    ct += 1  # increase count
                    break  # break out
            for a in a2:
                if pteq(a1[1], a):
                    ct += 1
                    break
            for a in a2:
                if pteq(a1[2], a):
                    ct += 1
                    break
            return ct

        # for index of face and face in the object's faces
        for i0, f0 in enumerate(faces):
            # set the face's index as key and make a list of indicies
            # for every index,face in faces
            for i1, f1 in enumerate(faces[i0:]):
                # if they share two points, append the index to the list of
                # faces adjacent to face at index i0
                if i1 != i0 and (ct_shrd_pts(f0, f1) == 2):
                    self.adj[i0].append(i1 + i0)
                    self.adj[i1 + i0].append(i0)
                    # a face can only be adjacent to three other faces because
                    # faces are triangles
                    if len(self.adj[i0]) == 3:
                        break

        # build the tree
        self.mktree(set(self.cfaces.keys()))

    def mktree(self, faceInSet):
        r = set()
        n = faceInSet.pop()
        next = []
        if not faceInSet:
            b = AxisAlignedBoundingBox(self.cfaces[n])
            if b not in self.tree:
                self.tree[b] = []
            self.tree[b].append(self.cfaces[n])
            self.root = b
            return b
        r.add(n)
        goal = len(faceInSet) / 2
        while len(r) <= goal:
            adjFac = filter(
                lambda x: x in faceInSet and x not in r, self.adj[n])
            toPush = adjFac[:min(3, goal - len(r) + 1)]
            r |= set(toPush)
            faceInSet -= set(toPush)
            next.extend(toPush)
            if len(r) <= goal:
                if len(next) > 0:
                    # need to fix this, can't pop from an empty list
                    n = next.pop(0)
                else:
                    n = faceInSet.pop()
                    r.add(n)
        rbox = self.mktree(r)
        lbox = self.mktree(faceInSet)
        b = AxisAlignedBoundingBox(
            [rbox.uBound, rbox.lBound, lbox.uBound, lbox.lBound])
        if b not in self.tree:
            self.tree[b] = []
        self.tree[b].extend([i for i in (rbox, lbox) if hash(i) != hash(b)])
        self.root = b
        return b

    def RayBoxIntersection(self, r, k=None):
        l = set()
        stk = [self.root]
        # which point in k?
        while len(stk) > 0:
            k = stk.pop()
            if isinstance(k, AxisAlignedBoundingBox) and k.rayIntersect(r):
                for v in self.tree[k]:
                    if v.__class__.__name__ == k.__class__.__name__:
                        stk.append(v)
                    else:
                        l.add(k)
        return l

    def SegBoxIntersection(self, r, k=None):
        if k is None:
            k = self.root
        l = set()
        if k.segmentIntersect(r):
            for v in self.tree[k]:
                if v.__class__.__name__ == k.__class__.__name__:
                    l |= self.SegBoxIntersection(r, v)
                else:
                    l.add(k)
        return l

    # mykey, otherTree, other okey
    # takes a box from myself and a box from the other tree
    def BoxBoxIntersection(self, myk, otherTree, ok):
        d = defaultdict(set)
        # print myk, ok
        if myk.aabbIntersect(ok):
            # print 'myk.aabbIntersect(ok):'
            for v1 in self.tree[myk]:
                for v2 in otherTree.tree[ok]:
                    if v1.__class__.__name__ == 'AxisAlignedBoundingBox' and v2.__class__.__name__ == 'AxisAlignedBoundingBox':
                        self._updated(self.BoxBoxIntersection(v1, otherTree, v2), d)
                    elif v1.__class__.__name__ != 'AxisAlignedBoundingBox' and v2.__class__.__name__ == 'AxisAlignedBoundingBox':
                        self._updated(self.BoxBoxIntersection(myk, otherTree, v2), d)
                    elif v1.__class__.__name__ != 'AxisAlignedBoundingBox' and v2.__class__.__name__ != 'AxisAlignedBoundingBox':
                        # i'm a box with a face in it that intersects with a
                        # box with a face in it
                        d[myk].add(ok)
                    elif v1.__class__.__name__ == 'AxisAlignedBoundingBox' and v2.__class__.__name__ != 'AxisAlignedBoundingBox':
                        self._updated(self.BoxBoxIntersection(v1, otherTree, ok), d)
                    else:
                        print 'NOTHING HAPPENED'
        return d

    def _updated(self, newdict, d):
        # print 'UPDATED CALLED!!!'
        for k in newdict:
            d[k] |= newdict[k]

    def getFaces(self, *args):
        '''Returns a list faces associated with the bounding box or boxes passed as params
        If a box has no faces, the an empty list is returned.'''
        #[triTriIntersect(face, o, eps) for o in otherFaces if not isinstance(o, AxisAlignedBoundingBox)]
        return chain(*[[face for face in self.tree[b] if not isinstance(face, AxisAlignedBoundingBox)]
                       for b in args if b in self.tree])
