#
# _valid.py
# valid combinations of intesection and obscurations in VRCC-3D+
#
from _intersection import intersections
from _obscuration import obscurations

valid = {
    'DC': ['nObs', 'nObs_c', 'nObs_e', 'pObs', 'pObs_c', 'eObs', 'eObs_c', 'cObs', 'cObs_c'],
    'EC': ['nObs', 'nObs_c', 'nObs_e', 'pObs', 'pObs_c', 'eObs', 'eObs_c', 'cObs', 'cObs_c'],
    'PO': ['pObs', 'pObs_c', 'pObs_e', 'eObs', 'eObs_c', 'cObs', 'cObs_c'],
    'EQ': ['eObs_e'],
    'TPP': ['eObs_c', 'eObs_e', 'cObs_c'],
    'TPPc': ['eObs', 'eObs_e', 'cObs'],
    'NTPP': ['cObs_c'],
    'NTPPc': ['cObs']}
