#
# relationship.py
# A class to handle ensuring valid VRCC-3D relationships
#
from _valid import valid
from _intersection import intConverse
from _obscuration import obsConverse


class Relationship:

    def __init__(self, intersect=None, obscure=None):
        # if intersect and obscure:
        #    if not obscure in valid[intersect]:
                # TODO better exception type here
        #        raise Exception
        self.intersection = intersect
        self.obscuration = obscure
        # print self.intersection, self.obscuration

    def __str__(self):
        if not self.obscuration:
            return self.intersection
        if not self.intersection:
            return self.obscuration
        return '_'.join((self.intersection, self.obscuration))

    def converse(self):
        return Relationship(intConverse[self.intersection], obsConverse[self.obscuration])
