#
# _intersection.py
# valid 3D intersection relationships in VRCC-3D+
#

intersections = ['DC', 'EC', 'EQ', 'NTPP', 'NTPPc', 'PO', 'TPP', 'TPPc']
intConverse = {'TPP': 'TPPc',
               'NTPP': 'NTPPc',
               'TPPc': 'TPP',
               'NTPPc': 'NTPP',
               None: None}
[intConverse.update({r: r}) for r in intersections if not r in intConverse]

intPred = {'IntBnd': set(['NTPPc', 'PO', 'TPPc']),
           'BndInt': set(['NTPP', 'PO', 'TPP']),
           'BndBnd': set(['EC', 'EQ'] + intersections[5:]),
           'BndExt': set(intersections[:2] + intersections[4:6] + [intersections[-1]]),
           'ExtBnd': set(intersections[:2] + [intersections[3]] + intersections[5:7])}


def relFilter(nonempty=[], empty=[]):
    possible = set(intersections)
    ints = set(intersections)
    for e in nonempty:
        possible = possible.intersection(intPred[e])
    for e in empty:
        possible = possible.intersection(ints.difference(intPred[e]))
    return possible
