#
# relationshipMatrix.py
# A class to handle finding the relationship matrix of a series of objects
#

from relationship import Relationship
from _intersection import intersections, intPred, relFilter
from _obscuration import obsFilter, _emptyPred, _nonEmptyPred, ObsFns
from PredicatePicker import PredicatePicker
import random

import numpy
import sys
sys.path.append('..')
import viewport as vp


class RelationshipMatrix:

    def __init__(self, objList, pbar=None):
        self.IntFns = {
            'BndBnd': 'bndBnd', 'BndInt': 'bndInt', 'IntBnd': 'intBnd',
            'BndExt': 'bndExt', 'ExtBnd': 'extBnd'}
        # for now, do naieve methods... There's gotta be a smart way to do this
        # 3D
        names = dict([(o.name, i) for i, o in enumerate(objList)])
        self.matrix = [[None] * len(objList) for i in names]
        for i in range(len(objList)):
            self.matrix[i][i] = Relationship('EQ', 'eObs_e')
        for i in range(0, len(objList)):
            for j in range(i + 1, len(objList)):
                if pbar:
                    pbar.setLabelText(
                        "Calculating Relationship: {}, {}".format(objList[i].name, objList[j].name))
                int3D = self.logicTree3D(objList[i], objList[j])
                obs2D = self.logicTree2D(objList[i], objList[j])
                #print int3D
                self.matrix[i][j] = Relationship(int3D, obs2D)
                int3D = self.logicTree3D(objList[j], objList[i])
                obs2D = self.logicTree2D(objList[j], objList[i])
                #self.matrix[j][i] = self.matrix[i][j].converse()
                self.matrix[j][i] = Relationship(int3D, obs2D)
                # Handle that some obscurations do NOT have unique converse
                # obscurations
                #self.matrix[j][i].obscuration = self.logicTree2D(
                #    objList[j], objList[i])
                if pbar:
                    pbar.setValue(pbar.value() + 1)

    def logicTree3D(self, o1, o2, initial=None):
        predicate = PredicatePicker(intPred)
        ints = intPred.keys()
        used = []
        empty = []
        nonempty = []
        # debug
        counter = 0
        if initial:
            possible = initial
        else:
            possible = relFilter(nonempty, empty)
        while len(possible) > 1:
            rel = (predicate.rankPredicates(possible)[0])
            # debug
            # print 'Iter:', counter,'Rel:', rel, 'Empty:',empty,
            # 'Nonempty:',nonempty,'Used:', used,'Possible:', possible
            [empty, nonempty][
                getattr(o1, self.IntFns[rel])(o2, vp.eps)].append(rel)
            used.append(rel)
            possible = possible.intersection(relFilter(nonempty, empty))
            # debug
            counter += 1
        # print 'Exiting logicTree3D with state:', 'Empty:',empty, 'Nonempty:',nonempty,'Used:', used,'Possible:', possible
        #print 'used:', used, 'empty:', empty, 'nonempty:', nonempty
        return possible.pop()

    def logicTree2D(self, o1, o2, initial=None):
        predicate = PredicatePicker(_nonEmptyPred)
        empty = []
        nonempty = []
        used = []
        if initial:
            possible = initial
        else:
            possible = obsFilter(nonempty, empty)  # if these are 'something'
        obss = set(ObsFns.keys())
        while len(possible) > 1:
            rel = (predicate.rankPredicates(possible)[0]) if len(predicate.rankPredicates(
                possible)) > 0 and predicate.rankPredicates(possible)[0] not in used else obss.pop()
            [empty, nonempty][ObsFns[rel](o1, o2)].append(rel)
            used.append(rel)
            obss.discard(rel)
            possible = possible.intersection(obsFilter(nonempty, empty))
        return possible.pop()
