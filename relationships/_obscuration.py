#
# _obscuration.py
# Valid Obscuration terms for VRCC-3D+
#

from geometry.unproject import unproject
from Polygon.Utils import tileEqual

obscurations = ['nObs',
                'pObs', 'pObs_c', 'pObs_e',
                'cObs', 'cObs_c',
                'eObs', 'eObs_c', 'eObs_e',
                'mObs', 'mObs_c']

obsConverse = {'pObs': 'pObs_c',
               'pObs_c': 'pObs',
               'eObs': 'eObs_c',
               'eObs_c': 'eObs',
               'cObs': 'cObs_c',
               'cObs_c': 'cObs',
               'mObs': 'mObs_c',
               'mObs_c': 'mObs',
               None: None}


def _makeConverse(rel):
    if '_c' in rel:
        return rel.split('_')[0]
    if '_e' in rel:
        return rel
    return rel + '_c'

[obsConverse.update({r: _makeConverse(r)})
 for r in obscurations if not r in obsConverse]

_nonEmptyPred = {'IntInt': set(obscurations[1:]),
                 'IntExt': set(obscurations[:4] + ['cObs', 'mObs']),
                 'ExtInt': set(obscurations[:4] + ['cObs_c', 'mObs_c']),
                 'Hides':  set(['pObs', 'pObs_e', 'eObs', 'eObs_e', 'cObs', 'mObs', 'mObs_c']),
                 'HiddenBy': set(['pObs_c', 'pObs_e', 'eObs_c', 'eObs_e', 'cObs_c', 'mObs', 'mObs_c'])}
# do the dont cares appear in both dicts?

_emptyPred = {'IntInt': set(['nObs']),
              'IntExt': set(['pObs', 'eObs', 'eObs_c', 'eObs_e', 'cObs_c', 'mObs_c']),
              'ExtInt': set(['pObs_c', 'eObs', 'eObs_c', 'eObs_e', 'cObs', 'mObs']),
              'Hides':  set(['nObs', 'pObs_c', 'eObs_c', 'cObs_c']),
              'HiddenBy': set(['nObs', 'pObs', 'eObs', 'cObs'])}


def obsFilter(nonempty=[], empty=[]):
    # this does NOT handle InFront
    # should handle cObs_e here
    possible = set(obscurations)
    for ne in nonempty:
        possible = possible.intersection(_nonEmptyPred[ne])
    for e in empty:
        possible = possible.intersection(_emptyPred[e])
    return possible


def hides(a, b):
    mesh = a.projection() & b.projection()
    if len(mesh) == 0:
        return False
    for poly in tileEqual(mesh, 5, 5):
        for cpoint in poly[-1]:
            ret = unproject(a, cpoint[0], cpoint[1]) >= unproject(
                b, cpoint[0], cpoint[1])
            #is a in front of b for contour point in poly
            if not(isinstance(ret, bool)):
                return ret[2]
        return ret


def hiddenBy(a, b):
    return hides(b, a)


ObsFns = {
    'IntInt': lambda o1, o2: ((o1.projection() & o2.projection()).area()) > 0.0,
    'IntExt': lambda o1, o2: ((o1.projection() - o2.projection()).area()) > 0.0,
    'ExtInt': lambda o1, o2: ((o2.projection() - o1.projection()).area()) > 0.0,
    'Hides':  hides,
    'HiddenBy': hiddenBy}
