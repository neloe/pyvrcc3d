#
# ID3.py
# Nathan Eloe
# An implementation of an ID3 based predicate picker
#
from math import log


class PredicatePicker:

    def __init__(self, nonEmptyRels):
        '''Initializes the Predicate Picker class
        :param nonEmptyRels: Matches predicates to non-empty relationships
        :type nonEmptyRels: dictionary
        ({predicate: (set of relationships where predicate is true)})
        '''
        self.nonEmptyRels = nonEmptyRels

    def entropy(self, S):
        '''Determines the entropy of the set S
        :param S: The set of relationships to determine the entropy for
        :type S: set
        :return: Entropy of S, a float
        '''
        if len(S) == 0:
            return 0.0
        denom = 1.0 / len(S)
        # determine the number of times each predicate is nonempty in the set S
        ratios = [len(S & self.nonEmptyRels[k]) * denom
                  for k in self.nonEmptyRels
                  if len(S & self.nonEmptyRels[k]) > 0]
        return sum([-1 * r * log(r, 2) for r in ratios], 0.0)

    def gain(self, S, pred):
        '''Determines the information gain for using the specified predicate on
        the set of relationships.
        :param S: The set of relationships to determine the information gain for
        :param pred: The predicate to find the information gain for
        :type S: set
        :type pred: str
        :return: Information gain, a float
        '''
        if len(S) == 0:
            return 0.0
        predicateTrue = S & self.nonEmptyRels[pred]
        predicateFalse = S - self.nonEmptyRels[pred]
        denom = 1.0 / len(S)
        gains = map(lambda x: len(x) * denom * self.entropy(x),
                    [predicateTrue, predicateFalse])
        return self.entropy(S) - sum(gains)

    def rankPredicates(self, S):
        '''Returns a sorted list of predicates based on the potential information
        gain.
        :param S: The set of relationships to return the sorted predicates for
        :rype S: set
        :return: Sorted list of predicates
        '''
        gains = {k: self.gain(S, k) for k in self.nonEmptyRels
                 if self.gain(S, k) > 0.0}
        return sorted(gains.keys(), key=lambda x: gains[x], reverse=True)
