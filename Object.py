# object.py
# A class to hold an object for vrcc3d
# Nathan Eloe

from numpy import array,cross,dot,sqrt
from OpenGL.GL import *
from OpenGL.arrays import vbo
from OpenGL.GL.shaders import *
import glumpy
import numpy

#TODO clean out commented code... good lord!
class Object:
    def __init__(self,vts,ind, color,name):
        self.name=name
        #the vertices: a list of numpy arrays
        self.vertices = array(vts,'f')
        self.indices = array(ind,'i')
        #self.shader=prog
        faces = self.indices.reshape(-1,3)
        #glUseProgram(self.shader)
        #self.posAttr = glGetAttribLocation(self.shader,'position')
        #self.colorAttr = glGetAttribLocation(self.shader,'color')
        #self.normAttr = glGetAttribLocation(self.shader,'normal')
        #glUseProgram(0)
        #Last but not least, center the object around it's center of mass
        self.translation = sum(self.vertices)/float(len(self.vertices))
        self.vertices-=self.translation
        self.spin=array([0.0,0.0,0.0],'f')
        self.orbit=array([0.0,0.0,0.0],'f')
        #self.vertices-=com
        self.color = array(color,'f')
	#generate face norms
	#print self.vertices.shape, self.indices.shape
	T = self.vertices[faces]
	#print T.shape
	N = numpy.cross(T[::,1]-T[::,0], T[::,2]-T[::,0])
	L = numpy.sqrt(N[:,0]**2+N[:,1]**2+N[:,2]**2)
	N /= L[:, numpy.newaxis]
	self.normals = numpy.zeros(self.vertices.shape)
	self.normals[faces[:,0]]+=N
	self.normals[faces[:,1]]+=N
	self.normals[faces[:,2]]+=N
	L = numpy.sqrt(self.normals[:,0]**2+self.normals[:,1]**2+self.normals[:,2]**2)
	self.normals /= L[:,numpy.newaxis]
	self.V = numpy.zeros(len(self.vertices),[('position', numpy.float32, 3),
                                 ('color', numpy.float32, 3),
                                 ('normal',   numpy.float32, 3)])
	self.V['position'] = self.vertices
	self.V['color'] = color[:3]
	self.V['normal'] = self.normals

	self.mesh = glumpy.graphics.VertexBuffer(self.V, self.indices)
	'''
        self.bufferinfo = list()
        index=0
        vertNorms = dict()
        counts = dict()
        facememo = dict()
        def facenorm(face):
	    c = cross(self.vertices[face[2]]-self.vertices[face[0]],
			self.vertices[face[1]]-self.vertices[face[0]])
	    norm = c/sqrt(dot(c,c))
	    def add(v):
		if v not in vertNorms:
		    vertNorms[v]=norm
		    counts[v]=1
		else:
		    vertNorms[v]+=norm
		    counts[v]+=1
	    map(add,face.flat)
	map(facenorm,faces)
        for i,v in enumerate(self.vertices):
            self.bufferinfo+=v.flat
            self.bufferinfo+=self.color.flat
            norm = vertNorms[i]/counts[i]
            try:
		self.bufferinfo+=(norm/sqrt(dot(norm,norm))).flat
	    except:
		#print norm
		print vertNorms[i]
		print counts[i]
		exit()
        self.bufferinfo = array(self.bufferinfo)
        self.stride = self.color.size*self.color.itemsize
        #assume 3 for the verts and 3 for the normal
        self.stride += self.vertices.itemsize * 6
        self.vbuf = vbo.VBO(self.bufferinfo)
        self.ibuf = vbo.VBO(self.indices,
                            target=GL_ELEMENT_ARRAY_BUFFER)
        self.cbuf = vbo.VBO(self.color)
	'''
    
    def scale(self,value):
        self.vertices*=value
    def render(self, shaderManaged=False):
        if not shaderManaged:
	    pass
            #glUseProgram(self.shader)
        try:
	    #print 'binding buffers'
            #self.vbuf.bind()
            #self.ibuf.bind()
            #self.cbuf.bind()
            try:
		#print 'pushing and rotating'
                glPushMatrix()
                glRotatef(self.orbit[0],1.,0.,0.)
		glRotatef(self.orbit[1],0.,1.,0.)
		glRotatef(self.orbit[2],0.,0.,1.)
                glTranslate(*self.translation)
                glRotatef(self.spin[0],1.,0.,0.)
		glRotatef(self.spin[1],0.,1.,0.)
		glRotatef(self.spin[2],0.,0.,1.)
                #glEnableVertexAttribArray(self.posAttr)
                #glEnableVertexAttribArray(self.colorAttr)
		#print 'drawing'
		'''
                glVertexAttribPointer(self.posAttr,3,GL_FLOAT,
                                      GL_FALSE,self.stride,self.vbuf)
                glVertexAttribPointer(self.colorAttr,4,GL_FLOAT,
                                      GL_FALSE,self.stride,
                                      self.vbuf+12)
		glVertexAttribPointer(self.normAttr,3,GL_FLOAT,
                                      GL_FALSE,self.stride,
                                      self.vbuf+28)
                glDrawElementsui(GL_TRIANGLES,self.ibuf)
		'''
		glEnable(GL_POLYGON_OFFSET_FILL)
		glPolygonOffset(1,1)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
		self.mesh.draw(GL_TRIANGLES, 'pnc')
            finally:
#                self.vbuf.unbind()
#                self.ibuf.unbind()
                glPopMatrix()
        finally:
            if not shaderManaged:
		    pass
                #glUseProgram(0)
