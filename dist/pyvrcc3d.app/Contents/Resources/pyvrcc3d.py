import sys
from PySide import QtCore, QtGui
from PySide.QtGui import QFileDialog, QMessageBox, QProgressDialog
from ui.vrcc3d import Ui_vrcc3d
from OpenGL.GL import *
from OpenGL.GLU import *
from Loader import Loader
from render.RenderGroup import RenderGroup
from relationships.relationshipMatrix import RelationshipMatrix
from ui.infoDialog import infoDialog


class viewer(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.ui = Ui_vrcc3d()
        self.ui.setupUi(self)
        #self.ui.legend.hide()
        #self.ui.tableWidget.hide()
        #self.ui.FancyDialog.hide()
        self.ui.cTransU.clicked.connect(self.slotCameraTUp)
        self.ui.cTransD.clicked.connect(self.slotCameraTDown)
        self.ui.cTransL.clicked.connect(self.slotCameraTLeft)
        self.ui.cTransR.clicked.connect(self.slotCameraTRight)
        self.ui.cZoomI.clicked.connect(self.slotCameraZoomI)
        self.ui.cZoomO.clicked.connect(self.slotCameraZoomO)
        self.ui.cRotateR.clicked.connect(self.slotCameraRotR)
        self.ui.cRotateL.clicked.connect(self.slotCameraRotL)
        self.ui.cOrbitU.clicked.connect(self.slotCameraOrbitU)
        self.ui.cOrbitD.clicked.connect(self.slotCameraOrbitD)
        self.ui.cOrbitR.clicked.connect(self.slotCameraOrbitR)
        self.ui.cOrbitL.clicked.connect(self.slotCameraOrbitL)
        self.ui.actionLoad.activated.connect(self.slotLoadFile)
        self.ui.oTransL.clicked.connect(self.slotObjeftTL)
        self.ui.oTransR.clicked.connect(self.slotObjeftTR)
        self.ui.oTransU.clicked.connect(self.slotObjeftTU)
        self.ui.oTransD.clicked.connect(self.slotObjeftTD)
        self.ui.oOrbitR.clicked.connect(self.slotObjectOR)
        self.ui.oOrbitL.clicked.connect(self.slotObjectOL)
        self.ui.oOrbitD.clicked.connect(self.slotObjectOD)
        self.ui.oOrbitU.clicked.connect(self.slotObjectOU)
        self.ui.oSpinU.clicked.connect(self.slotObjectSU)
        self.ui.oSpinD.clicked.connect(self.slotObjectSD)
        self.ui.oSpinL.clicked.connect(self.slotObjectSL)
        self.ui.oSpinR.clicked.connect(self.slotObjectSR)
        self.ui.FancyDialog.clicked.connect(self.updateRelationshipMatrix)
        self.ui.tableWidget.cellClicked.connect(self.slotShowCellContents)
        self.ui.legend.cellClicked.connect(self.setTransparencySlider)
        self.ui.transparencySlider.sliderMoved.connect(self.setObjectTransparency)
        self.pbar = QProgressDialog(self)
    #slots

    def setTransparencySlider(self, row, col):
        name = self.ui.legend.item(row, col).text()
        alpha = self.ui.objViewerWidget.renderGroup.info[name]['alpha']
        self.ui.transparencySlider.setSliderPosition(QtGui.QSlider.TickPosition(int(alpha * 100)))

    def setObjectTransparency(self, value):
        if self.ui.legend.currentItem():
            name = self.ui.legend.currentItem().text()
            alpha = value / 100.0
            self.ui.objViewerWidget.renderGroup.info[name]['alpha'] = alpha
            self.ui.objViewerWidget.updateGL()

    def updateRelationshipMatrix(self):
        self.ui.tableWidget.showGrid()
        n = len(self.polyhedrons)
        self.pbar.setValue(0)
        self.pbar.setAutoClose(True)
        self.pbar.setMaximum((n * (n - 1)) / 2)
        matrix = RelationshipMatrix(self.polyhedrons, self.pbar).matrix
        for i in range(len(matrix)):
            for j in range(len(matrix)):
                self.ui.tableWidget.setItem(i, j, QtGui.QTableWidgetItem(str(matrix[i][j])))

    def slotShowCellContents(self, row, col):
        mbox = infoDialog(self)
        rel = self.ui.tableWidget.item(row, col).text()
        intersection = rel.split('_')[0]
        obscuration = '_'.join(rel.split('_')[1:])
        mbox.setIntersectionInfo(intersection)
        mbox.setObscurationInfo(obscuration)
        #obscuration
        #build the rest of the string
        mbox.open()

    def slotLoadFile(self):
        fname = QFileDialog.getOpenFileName(self, 'File Open', '', '*.obj')
        if fname[0] == '':
            return
        #self.ui.objViewerWidget.objGroup = objLoader(fname[0]).group
        self.pbar.open()
        self.pbar.setAutoClose(False)
        self.polyhedrons = Loader(fname[0], pbar=self.pbar).polyhedrons
        self.ui.tableWidget.setRowCount(len(self.polyhedrons))
        self.ui.tableWidget.setColumnCount(len(self.polyhedrons))
        headers = [p.name for p in self.polyhedrons]
        self.ui.tableWidget.setHorizontalHeaderLabels(headers)
        self.ui.tableWidget.setVerticalHeaderLabels(headers)
        self.ui.objViewerWidget.renderGroup = RenderGroup(self.polyhedrons)
        self.ui.objViewerWidget.updateGL()
        self.updateRelationshipMatrix()
        self.ui.legend.setRowCount(len(self.polyhedrons))
        self.ui.legend.setColumnCount(1)
        self.ui.legend.setHorizontalHeaderLabels(['Legend'])
        self.ui.legend.showGrid()
        for i in range(len(self.polyhedrons)):
            color = map(int, map(lambda x: 256 * x,
                                 self.ui.objViewerWidget.renderGroup.info[self.polyhedrons[i].name]['color']))
            self.ui.legend.setItem(i, 0, QtGui.QTableWidgetItem(str(self.polyhedrons[i].name)))
            self.ui.legend.item(i, 0).setBackground(QtGui.QColor(*color))
        self.ui.legend.adjustSize()
        self.ui.tableWidget.adjustSize()
        self.ui.legend.show()
        self.ui.tableWidget.show()
        self.ui.FancyDialog.show()

    def slotCameraTUp(self):
        self.ui.objViewerWidget.cameraTranslate([0., .1, 0.])

    def slotCameraTDown(self):
        self.ui.objViewerWidget.cameraTranslate([0., -.1, 0.])

    def slotCameraTLeft(self):
        self.ui.objViewerWidget.cameraTranslate([-0.1, 0., 0.])

    def slotCameraTRight(self):
        self.ui.objViewerWidget.cameraTranslate([0.1, 0., 0.])

    def slotCameraZoomI(self):
        self.ui.objViewerWidget.cameraTranslate([0., 0., -.2])

    def slotCameraZoomO(self):
        self.ui.objViewerWidget.cameraTranslate([0., 0., .2])

    def slotCameraPanU(self):
        self.ui.objViewerWidget.cameraPanTilt([-.5, 0., 0.])

    def slotCameraPanD(self):
        self.ui.objViewerWidget.cameraPanTilt([.5, 0., 0.])

    def slotCameraPanL(self):
        self.ui.objViewerWidget.cameraPanTilt([0., -.5, 0.])

    def slotCameraPanR(self):
        self.ui.objViewerWidget.cameraPanTilt([0., .5, 0.])

    def slotCameraRotR(self):
        self.ui.objViewerWidget.cameraPanTilt([0., 0., -.5])

    def slotCameraRotL(self):
        self.ui.objViewerWidget.cameraPanTilt([0., 0., .5])

    def slotCameraOrbitU(self):
        self.ui.objViewerWidget.cameraOrbit([-2.0, 0.0, 0.0])

    def slotCameraOrbitD(self):
        self.ui.objViewerWidget.cameraOrbit([2.0, 0.0, 0.0])

    def slotCameraOrbitL(self):
        self.ui.objViewerWidget.cameraOrbit([0.0, -2.0, 0.0])

    def slotCameraOrbitR(self):
        self.ui.objViewerWidget.cameraOrbit([0.0, 2.0, 0.0])

    def slotObjeftTR(self):
        self.ui.objViewerWidget.objectTranslate([0.1, 0., 0.])

    def slotObjeftTL(self):
        self.ui.objViewerWidget.objectTranslate([-0.1, 0., 0.])

    def slotObjeftTU(self):
        self.ui.objViewerWidget.objectTranslate([0., 0.1, 0.])

    def slotObjeftTD(self):
        self.ui.objViewerWidget.objectTranslate([0., -0.1, 0.])

    def slotObjectOR(self):
        self.ui.objViewerWidget.objectOrbit([0., 2., 0.])

    def slotObjectOL(self):
        self.ui.objViewerWidget.objectOrbit([0., -2., 0.])

    def slotObjectOU(self):
        self.ui.objViewerWidget.objectOrbit([-2., 0., 0.])

    def slotObjectOD(self):
        self.ui.objViewerWidget.objectOrbit([2., 0., 0.])

    def slotObjectSR(self):
        self.ui.objViewerWidget.objectSpin([0., 2., 0.])

    def slotObjectSL(self):
        self.ui.objViewerWidget.objectSpin([0., -2., 0.])

    def slotObjectSU(self):
        self.ui.objViewerWidget.objectSpin([-2., 0., 0.])

    def slotObjectSD(self):
        self.ui.objViewerWidget.objectSpin([2., 0., 0.])


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = viewer()
    myapp.show()
    sys.exit(app.exec_())
