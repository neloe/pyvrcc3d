#
## ObsList.py
## author: Nate
## Generate a list of all the obscurations calculated by the new thingy
#

import sys
import glob
import os
from functools import partial
from tutils import getObs, getRCC


fpath1 = partial(os.path.join, os.path.join('objects', 'old'))
fpath2 = partial(os.path.join, os.path.join('objects', 'new'))

#bad files on objs dir
badfiles1 = ['bolt_and_nut.obj', 'p51_mustang.obj', 'EC eObs_c.obj',
             'PO eObs.obj', 'TPPc cObs.obj', 'EC pObs.obj', 'EC pObs_c.obj']
#bad files in newObjs dir
badfiles2 = ['DC pObs_c.obj', 'EC_nObs_e.obj', 'EC_cObs.obj', 'EC_nObs.obj',
             'EC_pObs.obj', 'DC_cObs_c.obj', 'DC_nObs_c.obj', 'EC_cObs_c.obj', 'EC_eObs.obj',
             'EQ_eObs_e.obj', 'PO_cObs_c.obj', 'PO_eObs.obj', 'PO_pObs_e.obj',
             'TPPC_eObs_e.obj', 'TPPc_eObs.obj', 'TPP_eObs_e.obj']

flist = set()
flist.update(glob.glob(fpath1('*.obj')))
flist.update(glob.glob(fpath2('*.obj')))

map(flist.discard, map(fpath1, badfiles1))
map(flist.discard, map(fpath2, badfiles2))

l = sorted(flist)
fcn = getObs
if len(sys.argv) == 2 and sys.argv[1] == '3':
    fcn = getRCC

for f in l:
    obss = map(str, fcn(f))
    if obss[0] not in os.path.basename(f) and obss[1] not in os.path.basename(f):
        print '***\t',
    else:
        print '   \t',
    print f, ':', ', '.join(obss)
