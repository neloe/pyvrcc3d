#
## \file Loader.py
## \brief A class to handle file loading (designed to be extensible to other file formats)
#
from geometry.Polyhedron import Polyhedron
from filetypes import obj
import numpy


class Loader:
    def __init__(self, fname, ftype='obj', pbar=None):
        #Define supported file types
        self.ftypes = {'obj': obj,
                       }
        self.polyhedrons = []
        if ftype in self.ftypes:
            if pbar:
                pbar.setValue(0)
                #guess length
                pbar.setMaximum(10)
                pbar.setLabelText('Loading file ' + fname)
            polys = self.ftypes[ftype].load(fname)
            if pbar:
                pbar.setMaximum(2 * len(polys) + 2 + (len(polys) * (len(polys) - 1)) / 2)
                pbar.setValue(2)

            def polyAppend(poly):
                self.polyhedrons += self.breakPoly(poly, pbar)
            map(polyAppend, polys)

        else:
            raise NotImplementedError('Filetype ' + ftype + ' not supported.  \
                                      Supported types are: ' + ' '.join(self.ftypes.keys()))

    def breakPoly(self, poly, pbar=None):
        if pbar:
            pbar.setLabelText("Splitting polyhedron " + poly.name)
        connected = lambda f1, f2: any([f in f2 for f in f1])
        localv = list()
        vstack = list()

        indexList = list()

        verts = poly.faces().copy().tolist()

        while len(verts) > 0:
            localv = [verts[0]]
            vstack = [list(verts.pop(0))]
            while len(vstack) > 0:
                v = vstack.pop(0)
                newvs = [x for x in verts if connected(v, x) and not x in localv]
                #newvs = filter(lambda x: connected(v, x) and x not in localv, verts)
                localv += newvs
                vstack += newvs
                verts = [x for x in verts if not x in newvs]
                #verts = filter(lambda x: x not in newvs, verts)
            indexList.append(localv)

        if len(indexList) == 1:
            if pbar:
                pbar.setValue(pbar.value() + 2)
            return [poly]
        if pbar:
            pbar.setValue(pbar.value() + 1)
            pbar.setLabelText('Generating list of ' + str(len(indexList)) + ' polyhedrons from ' + poly.name)
        self.count = -1

        def generatePoly(ilist):
            mapping = {}
            flatfaces = [i for s in ilist for i in s]
            uniqueIndices = list(set(flatfaces))
            newverts = poly.vertices[numpy.array(uniqueIndices)]
            self.count += 1
            return Polyhedron(poly.name + str(self.count), newverts, numpy.array([uniqueIndices.index(i) for i in flatfaces]))
        if pbar:
            pbar.setValue(pbar.value() + 1)
        return map(generatePoly, indexList)
