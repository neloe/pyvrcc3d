#
# infoDialog.py
# A stylized more information dialog for pyvrcc3d
#
import sys
sys.path.append('..')
import _moreInfoBase
from relationships._intersection import intPred
from relationships._obscuration import _emptyPred, _nonEmptyPred
from PySide import QtGui


class infoDialog(QtGui.QDialog):

    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.ui = _moreInfoBase.Ui_Dialog()
        self.ui.setupUi(self)

    def setIntersectionInfo(self, intersection):
        predicates = intPred.keys()
        self.ui.intersectTable.setRowCount(len(predicates))
        self.ui.intersectTable.setColumnCount(1)
        self.ui.intersectTable.setVerticalHeaderLabels(predicates)
        for i in range(len(predicates)):
            self.ui.intersectTable.setItem(
                i, 0, QtGui.QTableWidgetItem(str(intersection in intPred[predicates[i]])))
        self.ui.intersectTable.showGrid()
        self.ui.intersectLabel.setText(intersection)
        # self.ui.intersectTable.adjustSize()
        # self.adjustSize()

    def setObscurationInfo(self, obscuration):
        predicates = _emptyPred.keys()
        self.ui.obscurationLabel.setText(obscuration)
        self.ui.obscurationTable.setColumnCount(1)
        self.ui.obscurationTable.setVerticalHeaderLabels(
            predicates + ['InFront'])
        self.ui.obscurationTable.setRowCount(len(predicates) + 1)
        for i in range(len(predicates)):
            if obscuration in _emptyPred[predicates[i]] and obscuration in _nonEmptyPred[predicates[i]]:
                val = '*'
            elif obscuration in _emptyPred[predicates[i]]:
                val = 'False'
            else:
                val = 'True'
            self.ui.obscurationTable.setItem(i, 0, QtGui.QTableWidgetItem(val))
        if '_c' in obscuration:
            self.ui.obscurationTable.setItem(3, 0, QtGui.QTableWidgetItem('N'))
        elif '_e' in obscuration:
            self.ui.obscurationTable.setItem(3, 0, QtGui.QTableWidgetItem('E'))
        else:
            self.ui.obscurationTable.setItem(3, 0, QtGui.QTableWidgetItem('Y'))
        self.ui.obscurationTable.showGrid()
        # self.ui.obscurationTable.adjustSize()
        # self.adjustSize()
