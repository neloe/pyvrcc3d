from PySide.QtOpenGL import QGLWidget
from OpenGL.GL import *
from OpenGL.GLU import *
import numpy
import sys


sys.path.append('..')
import viewport as vp


class viewWidget(QGLWidget):

    def __init__(self, parent):
        QGLWidget.__init__(self, parent)
        self.renderGroup = None
        self.makeCurrent()
        self.camera = numpy.array([0., 0., 20.])
        glLightfv(GL_LIGHT0, GL_DIFFUSE, (1., 1., 1., 1.))
        glLightfv(GL_LIGHT0, GL_AMBIENT, (1., 1., 1., 1.))
        glLightfv(GL_LIGHT0, GL_SPECULAR, (1., 1., 1., 1.))
        glLightfv(GL_LIGHT0, GL_POSITION, (2., 2., 20., 0.))
        glEnable(GL_LIGHTING)
        glEnable(GL_LIGHT0)
        glEnable(GL_COLOR_MATERIAL)
        glDisable(GL_CULL_FACE)
        glShadeModel(GL_SMOOTH)
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (.25, .25, .25))
        glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, 1)
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, (0.1, 0.1, 0.1))

    def paintGL(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(vp.FOVY, vp.aratio(), vp.NEAR, vp.FAR)
        glMatrixMode(GL_MODELVIEW)
        glPushMatrix()
        self._moveCamera()
        if self.renderGroup:
            self.renderGroup.render()
        glPopMatrix()

    def resizeGL(self, w, h):
        glViewport(0, 0, w, h)
        vp.WIDTH = w
        vp.HEIGHT = h
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(vp.FOVY, vp.aratio(), vp.NEAR, vp.FAR)

    def initializeGL(self):
        glClearColor(.0, .0, .0, .7)
        glClearDepth(1.)
        glEnable(GL_DEPTH_TEST)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(vp.FOVY, vp.aratio(), vp.NEAR, vp.FAR)

    def _moveCamera(self):
        glTranslatef(*(-1 * self.camera))

    def cameraTranslate(self, tVec):
        # self.camera=self.camera+numpy.array(tVec)
        self.objectTranslate(-1 * numpy.array(tVec))

    # TODO convert camera pan/til to model transformation
    def cameraPanTilt(self, rVec):
        self.cameraPan += numpy.array(rVec)
        self.updateGL()

    def cameraOrbit(self, oVec):
        self.objectOrbit(-1 * numpy.array(oVec))

    def objectTranslate(self, tVec, objs=[]):
        def _translate(obj):
            obj['poly'].translate(tVec)
        if objs == []:
            self.renderGroup.translation += tVec
        else:
            [self.renderGroup.info[p]['poly'].translate(tVec) for p in objs]
        self.updateGL()

    def objectSpin(self, sVec, objs=[]):
        def _spin(obj):
            obj['poly'].spin(sVec)
        if objs == []:
            self.renderGroup.spin += sVec
        else:
            [self.renderGroup.info[p]['poly'].spin(sVec) for p in objs]
        self.updateGL()

    def objectOrbit(self, oVec, objs=[]):
        def _orbit(obj):
            obj['poly'].orbit(oVec)
        if objs == []:
            self.renderGroup.orbit += oVec
        else:
            [self.renderGroup.info[p]['poly'].orbit(oVec) for p in objs]
        self.updateGL()
