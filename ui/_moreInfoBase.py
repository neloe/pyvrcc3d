# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '_moreInfoBase.ui'
#
# Created: Thu Aug 09 13:35:20 2012
#      by: pyside-uic 0.2.14 running on PySide 1.1.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui


class Ui_Dialog(object):

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(176, 143)
        self.gridLayout = QtGui.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.intersectLabel = QtGui.QLabel(Dialog)
        self.intersectLabel.setObjectName("intersectLabel")
        self.gridLayout.addWidget(self.intersectLabel, 0, 0, 1, 1)
        self.obscurationTable = QtGui.QTableWidget(Dialog)
        self.obscurationTable.setObjectName("obscurationTable")
        self.obscurationTable.setColumnCount(0)
        self.obscurationTable.setRowCount(0)
        self.obscurationTable.horizontalHeader().setVisible(False)
        self.gridLayout.addWidget(self.obscurationTable, 1, 4, 1, 1)
        self.obscurationLabel = QtGui.QLabel(Dialog)
        self.obscurationLabel.setObjectName("obscurationLabel")
        self.gridLayout.addWidget(self.obscurationLabel, 0, 4, 1, 1)
        self.intersectTable = QtGui.QTableWidget(Dialog)
        self.intersectTable.setObjectName("intersectTable")
        self.intersectTable.setColumnCount(0)
        self.intersectTable.setRowCount(0)
        self.intersectTable.horizontalHeader().setVisible(False)
        self.gridLayout.addWidget(self.intersectTable, 1, 0, 1, 3)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 2, 4, 1, 1)
        spacerItem = QtGui.QSpacerItem(
            40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 1, 3, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(
            self.buttonBox, QtCore.SIGNAL("accepted()"), Dialog.accept)
        QtCore.QObject.connect(
            self.buttonBox, QtCore.SIGNAL("rejected()"), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate(
            "Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.intersectLabel.setText(QtGui.QApplication.translate(
            "Dialog", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.obscurationLabel.setText(QtGui.QApplication.translate(
            "Dialog", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
