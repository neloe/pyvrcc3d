#
# RenderGroup.py
# A class to handle rendering groups of Polyhedrons
#

from OpenGL.GL import glEnable, GL_POLYGON_OFFSET_FILL, glPolygonOffset, glPolygonMode, GL_TRIANGLES, GL_FILL, GL_FRONT_AND_BACK, glPushMatrix, glPopMatrix, glRotatef, glTranslate, glColor4f
import glumpy
import numpy
from colors import getColors


class RenderGroup:

    def __init__(self, polys):
        colorList = getColors(len(polys))
        self.baseVBOArray = [
            ('position', numpy.float32, 3), ('normal', numpy.float32, 3)]
        self.info = {
            p.name: {'color': colorList.pop(), 'alpha': 1.0, 'poly': p} for p in polys}
        #self.color = colorList.pop()
        #self.alpha = 0.25
        [self._makeVBO(p) for p in polys]
        glEnable(GL_POLYGON_OFFSET_FILL)
        glPolygonOffset(1, 1)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        #"global" transformations
        self.translation = numpy.array([0., 0., 0.])
        self.orbit = numpy.array([0., 0., 0.])
        self.spin = numpy.array([0., 0., 0.])

    def render(self):
        [self._renderPoly(p) for p in self.info]

    def _renderPoly(self, p):
        if self.info[p]['poly'].stateChanged:
            self._makeVBO(self.info[p]['poly'])
        glPushMatrix()
        tOrbit = self.info[p]['poly'].getOrbitAngle() + self.orbit
        tSpin = self.info[p]['poly'].getSpinAngle() + self.spin
        tTrans = self.info[p]['poly'].getTranslation() + self.translation
        color = self.info[p]['color']
        alpha = self.info[p]['alpha']
        glRotatef(tOrbit[0], 1., 0., 0.)
        glRotatef(tOrbit[1], 0., 1., 0.)
        glRotatef(tOrbit[2], 0., 0., 1.)
        glTranslate(*tTrans)
        glRotatef(tSpin[0], 1., 0., 0.)
        glRotatef(tSpin[1], 0., 1., 0.)
        glRotatef(tSpin[2], 0., 0., 1.)
        glColor4f(color[0], color[1], color[2], alpha)
        self.info[p]['vbo'].draw(GL_TRIANGLES, 'pn')
        glPopMatrix()

    def _makeVBO(self, p):
        V = numpy.zeros(len(p.vertices), self.baseVBOArray)
        V['position'] = p.vertices
        V['normal'] = p.normals
        p.stateChanged = False
        self.info[p.name]['vbo'] = glumpy.graphics.VertexBuffer(V, p.indices)
