#
# colors.py
# Algorithm(s) to get distinct colors
#
import numpy
import colorsys


# algorithm borrowed from
# http://stackoverflow.com/questions/470690/how-to-automatically-generate-n-distinct-colors
def getColors(n):
    colors = []
    for i in numpy.arange(0., 360., 360. / n):
        hue = i / 360.
        lightness = (50 + numpy.random.rand() * 10) / 100
        saturation = (90 + numpy.random.rand() * 10) / 100
        colors.append(list(colorsys.hls_to_rgb(hue, lightness, saturation)))
    return colors
