#
# \file obj.py
# \brief Handles loading Wavefront OBJ files
#
import sys
if '..' not in sys.path:
    sys.path.append('..')
from geometry.Polyhedron import Polyhedron
from numpy import array

_currG = None


def load(fname):
    '''Loads a wavefront obj file and returns a list of Polyedrons'''
    f = open(fname)
    verts = list()
    faces = dict()

    def ffunc(t):
        if _currG not in faces:
            faces[_currG] = list()
        face = [int(i.split('/')[0]) - 1 for i in t[1:]]
        faces[_currG] += [[face[0]] + face[i:i + 2]
                          for i in range(1, len(face) - 1)]

    def gfunc(tok):
        global _currG
        _currG = tok[1].lower()

    def vfunc(t):
        verts.append(map(float, t[1:]))

    handlers = {'g': gfunc,
                'v': vfunc,
                'f': ffunc,
                }

    def parseLine(line):
        tokens = filter(None, line.strip().split())
        if not tokens:
            return None
        tokens[0] = tokens[0].lower()
        if tokens:
            try:
                handlers[tokens[0]](tokens)
            except Exception as e:
                pass
    # for line in f:
        # parseLine(line)
    map(parseLine, f)
    # todo handle breaking of objects here... Perhaps user configurable?
    objVerts = dict()

    def zeroIndexVerts(key):
        flatface = [i for s in faces[key] for i in s]
        objVerts[key] = [verts[i] for i in set(flatface)]
        faces[key] = [objVerts[key].index(verts[i]) for i in flatface]

    map(zeroIndexVerts, faces)
    return [Polyhedron(name, array(objVerts[name]), array(faces[name])) for name in faces]
