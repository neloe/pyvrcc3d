#
## viewport.py
## some constants for the viewport and camera position
#
from math import tan, radians
import numpy
import sys

WIDTH = 401.
HEIGHT = 301.
FOVY = 40.
NEAR = 1.
FAR = 60.
CAM_POS = numpy.array([0., 0., 20.])


def aratio():
    return float(WIDTH) / HEIGHT


def depth(point):
    return CAM_POS[2] - point[2]


def iPlane(point):
    return CAM_POS[2] - point[2] - NEAR


tmemo = {}


def memoTAN():
    return tmemo.setdefault(FOVY, 2. * tan(radians(FOVY * .5)) / HEIGHT)


def eps(point):
    return depth(point) * memoTAN()
